#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main()
{
    double x, result;
    cin >> x;
    if (x > -5 && x <= 10)
    {
        result = pow(6, (-2 * x)) + 5;
    }
    else if (x > 10)
    {
        result = 4 * pow(x, 3) - (1.0 / cbrt(8 - x));
    }
    else
    {
        result = (4 * x) / abs(pow(x, 2) - 16);
    }
    cout << fixed;
    cout.precision(15);
    cout << result << endl;
    return 0;
}