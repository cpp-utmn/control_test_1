#include <iostream>
using namespace std;

bool func(int a, int b)
{
    if (b == 0)
    {
        return 0;
    }
    else
    {
        return a % b == 0;
    }
}

int main()
{
    int a, b;
    cin >> a >> b;
    if (func(a, b))
    {
        cout << "1" << endl;
    }
    else
    {
        cout << "0" << endl;
    }
    return 0;
}
