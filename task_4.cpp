#include <iostream>
#include <cmath>

int main()
{
    double m;
    std::cin >> m;
    double a1 = 2.0;
    double a2 = a1 - std::sin(a1 + 1.0);
    int n = 2;
    while (std::abs(a2 - a1) >= m)
    {
        a1 = a2;
        a2 = a1 - std::sin(a1 + 1.0);
        n++;
    }
    std::cout << n << std::endl;
    return 0;
}
