#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    double sum = 0, count = 0;
    int minim;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] % 11 == 0 && arr[i] > 0)
        {
            sum += arr[i];
            count++;
        }
        else if (i % 2 == 0 && arr[i] < minim)
        {
            minim = arr[i];
        }
    }

    if (count > 0)
    {
        cout << sum / count << endl;
    }
    else
    {
        cout << minim << endl;
    }
    return 0;
}
