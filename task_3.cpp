#include <iostream>

int main()
{
    double a;
    std::cin >> a;

    double sum = 0;
    int n = 1;
    while (sum <= a)
    {
        sum += 1.0 / n;
        n++;
    }

    std::cout << n - 1 << std::endl;

    return 0;
}
