#include <iostream>
#include <cmath>

int main()
{
    int n;
    std::cin >> n;

    double sum = 0.0;
    double factorial = 1.0;
    for (int k = 1; k <= n; k++)
    {
        factorial *= k;
        sum += factorial / pow(k, 5);
    }

    std::cout << sum << std::endl;

    return 0;
}
